public class Converter {
		//Your names go here:
		/*
		* @Author: Sijie
		* Mohamed
		* Christian
		*
		*/
		private static double celsiusToFahrenheit(double C){
            C=(((C*9)/5)+32);
            return C;
		}
		private static double fahrenheitToCelsius(double F){
		 F=(((F-32)*5)/9);
		return F;
		}
		public static void main(String[] args) {
		//TODO: The first student will implement this method.
		// Call CelsiusToFahrenheit to convert 180 Celsius to Fahrenheit value.
		double celsius = celsiusToFahrenheit(180);
		System.out.println("180 Celsius is "+celsius+" Fahrenheit.");
		// Call FahrenheitToCelsius to convert 250 Fahrenheit to Celsius value.
		double fahrenheit = fahrenheitToCelsius(250);
		System.out.println("250 Fahrenheit is "+fahrenheit+" Celsius.");
}
}
